var express = require('express');
var path = require('path');
var mongo = require('mongodb');
var bodyParser = require('body-parser');
var expressValidator = require('express-validator');
var session = require('express-session');
var cookie = require('cookie-parser');
var passport = require('passport')
var LocalStrategy = require('passport-local').Strategy;
var exphbs = require('express-handlebars');
var router = express.Router();
var flash = require('connect-flash');
var User = require('./public/js/sch.js');
var nodemailer = require('nodemailer');
var mongoose = require('mongoose');

var app = express();
app.engine('handlebars', exphbs());

  app.set('view engine', 'handlebars');


//Middlewares
app.use(session({ cookie: { maxAge: 60000 },
	secret: 'secret',
	saveUninitialized: true,
	resave: false
}));
app.use(flash());

app.use(passport.initialize());
  app.use(passport.session());

app.use(expressValidator({
	errorFormatter: function(param,msg,value){
		var namespace = param.split('.')
		, root = namespace.shift() 
		, formParam = root;

		while(namespace.length) {
			formParam += '[' + namespace.shift() +']';
		}
		return {
			param : formParam,
			msg : msg,
			value : value
		};
	}
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: false
}));

app.use('/',router);



app.use(expressValidator());

app.use(express.json());


app.use('/public', express.static(__dirname + '/public'));
app.use(bodyParser.json());
//var new_db = "mongodb://localhost:27017/detail";

app.get('/', function (req, res) {
	res.set({
		'Access-Control-Allow-Origin': '*'
	});
	return res.render('index');
}).listen(3000);

console.log("Server listening at : 3000");



// Sign-up 
router.post('/sign_up', function (req, res) {
	
	var email = req.body.email;
	var password = req.body.password;
	var cpasssword = req.body.cpassword;


	req.checkBody('email', 'email is required').notEmpty();
	req.checkBody('email', 'email is not valid').isEmail();
	req.checkBody('password', 'password is required').notEmpty();
	req.checkBody('cpassword', 'confirm password is required').notEmpty();
	req.checkBody('cpassword', 'password do not match').equals(req.body.password);

		var errors = req.validationErrors();

	if(errors) {
		console.log('error is present please check fields');
		res.render('index');
		
	} else {
		console.log('no errors');
		var data = new User({

			"email": email,
			"password": password,
			"cpassword": cpasssword
		});

		data.save(function(errors){
			if(errors){
				res.redirect('/');
				console.log('error');
				return;	
			} else {
				res.redirect('/public/success.html');
				console.log(data);
		}
		})}
});

	
app.post('/login',
  passport.authenticate('local', { successRedirect: '/public/redirect.html',
								   failureRedirect: '/',
								   failureFlash: true
                                    })
);

passport.use(new LocalStrategy(
	{usernameField:"email", passwordField:"password"},
	function (email, password, done) {
		
		User.findOne({ email: email }, function(err, user) {
			if (err) { return done(err); }
			if (!user) {
				console.log('user nhi h');
			  return done(null, false, { message: 'Incorrect username.' });
			}
			if (user.password != password) {console.log('pass nhi h');
			return done(null, false, { message: 'Incorrect password.' });  } 
				
			
			return done(null, user);
		  });
	}
));

passport.serializeUser(function(user, done) {
	done(null, user);
  });
  
  passport.deserializeUser(function(user, done) {
	done(null, user);
  });

//  forget password section 

router.post('/forget',(req,res) =>{

	
	var onEmail = `${req.body.email}`;

	var emailData = "testing";
	
let transporter = nodemailer.createTransport({
	host: 'smtp.mailtrap.io',
	port: 2525,
	secure: false, 
	auth: {
		user: 'c662044485266d', 
		pass: '7ecc66ea5bea1c' 
	}
});

// setup email data
let mailOptions = {
	from: '"Support Team" <7f6a3924a6-45c403@inbox.mailtrap.io>', 
	to: onEmail, 
	subject: 'Hello Here Is Your Password',
	text: 'Hello world?',
	html: emailData 
};


transporter.sendMail(mailOptions, (error, info) => {
	if (error) {
		return console.log(error);
	}
	console.log('Message sent: %s', info.messageId);
	// Preview only available when sending through an Ethereal account
	console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
	res.render('index',{msg: 'Your Passwor has been sent'});
	
	
});
})