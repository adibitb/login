//Object for Personal Details page containing various DOM reference
let personal_details = {
    button: document.getElementById('submit'),
    button_handler: function() {
        this.button.addEventListener('click', checkInputText);
    },
    text_inputs: (function(){
            let inputs = Array.prototype.slice.call(document.querySelectorAll('input'));
            return inputs.filter(item => item.getAttribute('type') == 'text');
        })(),
    selection: document.querySelectorAll('select')
};

//in-built form validation
function checkInputText() {
    count = counter();
    if(count == personal_details.text_inputs.length) {
        formValidation();
        personal_details.button.addEventListener('click', formValidation);
        personal_details.button.removeEventListener('click', checkInputText);
    }
}

//manual form validation
function formValidation() {
    count = counter();
    if(count != personal_details.text_inputs.length) {
        checkInputText();
        personal_details.button.addEventListener('click', checkInputText);
        personal_details.button.removeEventListener('click', formValidation);
    }
    else {
        for(let i=0; i<personal_details.selection.length; i++) {
            if(personal_details.selection[i].value == '') {
                alert('Please select '
                    + personal_details.selection[i].firstElementChild.textContent.toLowerCase()
                    + '!');
                break;
            }
        }
    }
}

function counter() {
    let count = 0;
    for(let i=0; i<personal_details.text_inputs.length; i++) {
        if(personal_details.text_inputs[i].value == '')
            break;
        count += 1;
    }
    return count;
}

window.onload = () => {
    personal_details.button_handler();
};