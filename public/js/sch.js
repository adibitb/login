var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/detaile', { useNewUrlParser: true });

var db = mongoose.connection;

var UserSchema = new mongoose.Schema({
  email: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  password: {
    type: String,
    required: true,
  },
  cpassword: {
    type: String,
    required: true,
  }
});
var User = module.exports= mongoose.model('User', UserSchema);
